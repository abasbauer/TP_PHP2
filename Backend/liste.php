<?php

$listClients = [
	"CNAMGS",
	"CNSS",
	"Gabon Telecoms"
];
$listPresta = [
	"Gabon Meca",
	"Gabon Aluminium",
	"Bois du Gabon"
];

$choix = $_GET["choix"];

if($choix=="clients") {
	echo json_encode($listClients);
	exit();
}

if($choix=="prestataires") {
	echo json_encode($listPresta);
	exit();
}